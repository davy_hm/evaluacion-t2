﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET2_CASO_01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Empresa ABC");
            Stack<Nodo> ListaProgramas = new Stack<Nodo>();
            ListaProgramas.Push(new Nodo("Programa 1", "Suma"));
            ListaProgramas.Push(new Nodo("Programa 2", "Resta"));
            ListaProgramas.Push(new Nodo("Programa 3", "Multiplicacion"));
            ListaProgramas.Push(new Nodo("Programa 4", "División"));

            Console.WriteLine("\nLista de procesos pendientes:\n-----------------------------");
            foreach (var elem in ListaProgramas)
            {
                Console.WriteLine(elem.nombre + " TIPO: " + elem.tipo);
            }

            Console.WriteLine("\n...Procesando 2 últimos programa ingresados...");
            ListaProgramas.Pop();
            ListaProgramas.Pop();

            Console.WriteLine("\nLista de procesos pendientes:\n-----------------------------");
            foreach (var elem in ListaProgramas)
            {
                Console.WriteLine(elem.nombre + " TIPO: " + elem.tipo);
            }

            Console.WriteLine("\nIngresando nuevo programa...");
            ListaProgramas.Push(new Nodo("Programa 5", "Potencia"));
            Console.WriteLine("\nLista de procesos pendientes:\n-----------------------------");
            foreach (var elem in ListaProgramas)
            {
                Console.WriteLine(elem.nombre + " TIPO: " + elem.tipo);
            }



            Console.WriteLine("\n...Procesando último programa ingresado...");
            ListaProgramas.Pop();

            Console.WriteLine("\nLista de procesos pendientes:\n-----------------------------");
            foreach (var elem in ListaProgramas)
            {
                Console.WriteLine(elem.nombre + " TIPO: " + elem.tipo);
            }
            Console.ReadKey();
        }

        class Nodo
        {
            public string nombre;
            public string tipo;

            public Nodo(string nombre, string tipo)
            {
                this.nombre = nombre;
                this.tipo = tipo;
            }
        }
    }
}
