﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ET2_Caso_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Consultora: Yuki Consulting\n");
            ColadePrioridad.Nodo ColaP;
            ColaP = ColadePrioridad.Nuevo("Juan", "Ramirez", "N0010P", 1);
            ColaP = ColadePrioridad.push(ColaP, "Marco", "Polo", "N0025A", 2);
            ColaP = ColadePrioridad.push(ColaP, "Hugo", "Cuba", "N0075E", 3);
            ColaP = ColadePrioridad.push(ColaP, "Ana", "Saenz", "N00251B", 4);
            ColaP = ColadePrioridad.push(ColaP, "Carlos", "Tupac", "N0018C", 5);

            ColadePrioridad.mostrar(ColaP);

            Console.WriteLine("\nAtendiendo primero de la cola...");
            ColaP = ColadePrioridad.pop(ColaP);

            ColadePrioridad.mostrar(ColaP);

            Console.WriteLine("\nIngresando alumno preferencial....");
            ColaP = ColadePrioridad.push(ColaP, "Alberto", "Rodriguez", "N0011A", 0);

            ColadePrioridad.mostrar(ColaP);

            Console.WriteLine("\nAtendiendo primero de la cola...");
            ColaP = ColadePrioridad.pop(ColaP);

            ColadePrioridad.mostrar(ColaP);


            Console.ReadKey();
        }
    }
    class ColadePrioridad
    {
        public class Nodo
        {
            public string nombre, apellido, id;
            public int prioridad;
            public Nodo siguiente;
        }

        public static Nodo Nuevo(string nombre, string apellido, string id, int prioridad)
        {
            Nodo temp = new Nodo() { nombre = nombre, apellido = apellido, id = id, prioridad = prioridad, siguiente = null };
            return temp;
        }

        public static Nodo pop(Nodo cabeza)
        {
            cabeza = cabeza.siguiente;
            return cabeza;
        }
        public static Nodo push(Nodo cabeza, string nombre, string apellido, string id, int prioridad)
        {
            Nodo puntero = cabeza;
            Nodo temp = Nuevo(nombre, apellido, id, prioridad);
            if (cabeza.prioridad > prioridad)
            {
                temp.siguiente = cabeza;
                cabeza = temp;
            }
            else
            {
                while (puntero.siguiente != null && puntero.siguiente.prioridad < prioridad)
                {
                    puntero = puntero.siguiente;
                }
                temp.siguiente = puntero.siguiente;
                puntero.siguiente = temp;
            }
            return cabeza;
        }
        public static void mostrar(Nodo cabeza)
        {
            Nodo temp = cabeza;
            if (temp == null)
            {
                Console.WriteLine("Cola Vacía");
            }
            Console.WriteLine("\nLista de pendientes:\n-----------------------");
            while (temp != null)
            {
                Console.WriteLine("ID: " + temp.id + "\tNombres: " + temp.nombre + " " + temp.apellido);
                temp = temp.siguiente;
            }
        }
    }
}
